import TokenType.*
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Test

class LexerTest {
    @Test
    fun justIdent() {
        match("abzB_A_", arrayOf(IDENT))
    }

    @Test
    fun justNumber() {
        match("259", arrayOf(NUMBER))
    }

    @Test
    fun simpleArithmetic() {
        val expected = arrayOf(
            LPAREN,
            LPAREN, NUMBER, PLUS, NUMBER, RPAREN,
            EQ, NUMBER,
            RPAREN
        )
        match("((2+2)=4)", expected)
    }

    @Test
    fun ifCondition() {
        val expected = arrayOf(
            LBRACKET,
            LPAREN,
            LPAREN, NUMBER, PLUS, NUMBER, RPAREN,
            MORE,
            LPAREN, NUMBER, PLUS, NUMBER, RPAREN,
            RPAREN,
            RBRACKET,
            QUEMARK, LBRACE, NUMBER, RBRACE,
            COLON, LBRACE, NUMBER, RBRACE
        )
        match("[((10+20)>(20+10))]?{1}:{0}", expected)
    }

    @Test
    fun simpleProgram() {
        val expected = arrayOf(
            IDENT, LPAREN, IDENT, RPAREN,
            EQ_LBRACE, IDENT, RBRACE, EOL,
            IDENT, LPAREN, NUMBER, RPAREN
        )
        match("g(x)={x}\ng(10)", expected)
    }

    private fun match(source: String, expected: Array<TokenType>) {
        val lexer = Lexer(source);
        val actual = getAllTokens(lexer)
        assertArrayEquals(actual, expected)
    }

    private fun getAllTokens(lexer: Lexer): Array<TokenType> {
        val list = mutableListOf<TokenType>()
        var reachedEOF = false
        while (!reachedEOF) {
            val token = lexer.fetch()
            if (token.type == TokenType.EOF) {
                reachedEOF = true
            } else {
                list.add(token.type)
            }
        }
        return list.toTypedArray()
    }
}