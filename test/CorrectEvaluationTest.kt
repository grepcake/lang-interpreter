import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CorrectEvaluationTest {
    @Test
    fun addition() {
        val text = "(2+2)"
        assertEquals(4, eval(text))
    }

    @Test
    fun subtraction() {
        val text = "((1+5)-8)"
        assertEquals(-2, eval(text))
    }

    @Test
    fun multiplication() {
        val text = "(6*8)"
        assertEquals(48, eval(text))
    }

    @Test
    fun division() {
        val noRemTest = "(81/9)"
        val remTest = "(104/14)"
        assertEquals(9, eval(noRemTest))
        assertEquals(7, eval(remTest))
    }

    @Test
    fun remainder() {
        val noRemTest = "(81%9)"
        val remTest = "(104%14)"
        assertEquals(0, eval(noRemTest))
        assertEquals(6, eval(remTest))
    }

    @Test
    fun less() {
        val lessText = "((1-4)<-1)"
        val equalText = "(2<2)"
        val greaterText = "(-1<-5)"
        assertEquals(1, eval(lessText))
        assertEquals(0, eval(equalText))
        assertEquals(0, eval(greaterText))
    }

    @Test
    fun complexArithmetic() {
        val text = "(5+((3*4)/5))"
        assertEquals(7, eval(text))
    }

    @Test
    fun ifExpression() {
        val text = "[((10+20)>(20+10))]?{1}:{0}"
        assertEquals(0, eval(text))
    }

    @Test
    fun simpleFunctions() {
        val sumFunc = "f(x,y)={(x+y)}"
        testBiFunction(Int::plus, sumFunc, 0..100, 0..100)
        val diffFunc = "f(x,y)={(x-y)}"
        testBiFunction(Int::minus, diffFunc, 0..100, 0..100)
        val multFunc = "f(x,y)={(x*y)}"
        testBiFunction(Int::times, multFunc, 0..100, 0..100)
        val divFunc = "f(x,y)={(x/y)}"
        testBiFunction(Int::div, divFunc, 0..100, 1..100)
    }

    @Test
    fun fibonacciFunction() {
        val fibBody = "fib(x)={[(x>1)]?{(fib((x-1))+fib((x-2)))}:{x}}\n"
        val iterCount = 10
        val fibList = mutableListOf(0, 1)
        for (i in 0..iterCount - 2) {
            fibList.add(fibList.takeLast(2).sum())
        }
        val expected = fibList.toIntArray()
        val actual = (0..iterCount).map { eval(fibBody + "fib($it)") }.toIntArray()
        assertArrayEquals(expected, actual)
    }

    @Test
    fun severalFunctions() {
        val text = """
    g(x)={(f(x)+f((x/2)))}
    f(x)={[(x>1)]?{(f((x-1))+f((x-2)))}:{x}}
    g(10)
        """.trimIndent()
        assertEquals(60, eval(text))
    }

    private fun testBiFunction(func: (Int, Int) -> Int, langFunc: String, args1: Iterable<Int>, args2: Iterable<Int>) {
        val grouped = args1.flatMap { arg1 -> args2.map { arg1 to it } }
        val expected = grouped.map { (arg1, arg2) -> func(arg1, arg2) }.toIntArray()
        val actual = grouped.map { (arg1, arg2) -> eval("$langFunc\nf($arg1, $arg2)") }.toIntArray()
        assertArrayEquals(expected, actual)
    }

    private fun eval(text: String): Int {
        val program = Parser(text).parse()
        return program.evaluate()
    }
}
