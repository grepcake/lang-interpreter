sealed class LangException(protected val loc: Location) : Exception() {
    override val message: String
        get() = "%s: %s\n%s".format(errorName, position, loc.highlight())


    abstract val errorName: String
    open val position = "%s:%d".format(loc.getValue(), loc.lineNo)
}

class LangSyntaxError(loc: Location) : LangException(loc) {
    override val message: String
        get() = "%s: line #%d\n%s".format(errorName, loc.lineNo, loc.getLine())
    override val errorName = "SYNTAX ERROR"
    override val position = ""
}

class FunctionNotFoundException(loc: Location) : LangException(loc) {
    override val errorName = "FUNCTION NOT FOUND"
}

class ParameterNotFoundException(loc: Location) : LangException(loc) {
    override val errorName = "PARAMETER NOT FOUND"
}

class ArgumentMismatchException(loc: Location) : LangException(loc) {
    override val errorName = "ARGUMENT NUMBER MISMATCH"
}

class LangRuntimeError(loc: Location) : LangException(loc) {
    override val errorName = "RUNTIME ERROR"
}

class NotCallableException(loc: Location) : LangException(loc) {
    override val errorName = "NOT CALLABLE"
}
