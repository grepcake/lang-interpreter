class Token(val type: TokenType, lineNo: Int, source: String, start: Int, end: Int) {
    internal val loc = Location(source, start, end, lineNo)
}

