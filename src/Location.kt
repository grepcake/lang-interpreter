class Location(val source: String, val start: Int, val end: Int, val lineNo: Int) {
    fun getValue() = source.substring(start, end)

    private fun getLineLoc(): Location {
        val lineStart = source.substring(0, start).lastIndexOf('\n') + 1
        var lineEnd = source.indexOf('\n', end)
        if (lineEnd == -1) {
            lineEnd = source.length
        }
        return Location(source.substring(lineStart, lineEnd), start - lineStart, end - lineStart, lineNo)
    }

    fun highlight(): String {
        val lineLoc = getLineLoc()
        val linePrefix = "(%d:%d) ".format(lineNo, lineLoc.start + 1)
        val blanksCount = linePrefix.length + lineLoc.start
        val indicatorLine = " ".repeat(blanksCount) +
                "^".repeat(Integer.max(1, lineLoc.end - lineLoc.start))
        return linePrefix + lineLoc.source + '\n' + indicatorLine
    }

    fun getLine() = getLineLoc().source
}

fun pointLocation(loc: Location): Location {
    return Location(loc.source, loc.start, loc.start, loc.lineNo)
}