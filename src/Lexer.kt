import kotlin.math.min

class Lexer(private val source: String) {
    private var pos = 0
    private var lineNo = 1
    private val stack = mutableListOf<Pair<Int, Int>>()

    fun fetch(): Token {
        val first = consume()
        val start = pos
        val tokenType = when {
            first == null -> TokenType.EOF
            isCharacter(first) -> {
                var ch = next()
                while (ch != null && isCharacter(ch)) {
                    ch = next()
                }
                --pos
                TokenType.IDENT
            }
            isDigit(first) -> {
                var ch = next()
                while (ch != null && isDigit(ch)) {
                    ch = next()
                }
                --pos
                TokenType.NUMBER
            }
            isEOL(first) -> {
                ++lineNo
                TokenType.EOL
            }
            else -> when (first) {
                '+' -> TokenType.PLUS
                '-' -> TokenType.MINUS
                '*' -> TokenType.STAR
                '/' -> TokenType.SLASH
                '%' -> TokenType.PERCENT
                '<' -> TokenType.LESS
                '>' -> TokenType.MORE
                '=' -> {
                    if (next() == '{') {
                        TokenType.EQ_LBRACE
                    } else {
                        --pos
                        TokenType.EQ
                    }
                }
                '{' -> TokenType.LBRACE
                '}' -> TokenType.RBRACE
                '(' -> TokenType.LPAREN
                ')' -> TokenType.RPAREN
                ',' -> TokenType.COMMA
                '[' -> TokenType.LBRACKET
                ']' -> TokenType.RBRACKET
                '?' -> TokenType.QUEMARK
                ':' -> TokenType.COLON
                else -> TokenType.UNKNOWN
            }
        }
        next()
        return Token(tokenType, lineNo, source, start, pos)
    }

    private fun consume(): Char? {
        while (pos < source.length && isWhitespace(source[pos])) {
            ++pos
        }
        if (pos == source.length) {
            return null
        }
        return source[pos]
    }

    private fun next(): Char? {
        pos = min(source.length, pos + 1)
        if (pos < source.length) {
            return source[pos]
        } else {
            return null
        }
    }

    private fun isWhitespace(c: Char) = c.isWhitespace() && !isEOL(c)

    private fun isEOL(c: Char) = c == '\n'

    private fun isCharacter(c: Char) = c in CHARACTERS

    private fun isDigit(c: Char) = c in DIGITS

    fun backup() {
        stack.add(pos to lineNo)
    }

    fun restore() {
        if (stack.isNotEmpty()) {
            pos = stack.last().first
            lineNo = stack.last().second
            stack.removeAt(stack.size - 1)
        }
    }

    companion object {
        const val CHARACTERS = "abcdefghijklmnopqrstuvwzyxABCDEFGHIJKLMNOPQRSTUVWZYX_"
        const val DIGITS = "0123456789"
    }
}