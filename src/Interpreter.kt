import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStream

const val USAGE = "USAGE: Interpreter [-i/file]\n" +
        "Evaluate program from file or from stdin if '-i' is sole argument\n"

fun main(args: Array<String>) {
    if (args.size != 1) {
        println(USAGE)
        return
    }
    val inputStream: InputStream
    if (args[0] == "-i") {
        inputStream = System.`in`
    } else {
        try {
            inputStream = FileInputStream(args[0])
        } catch (error: FileNotFoundException) {
            System.err.println("Can't open '${args[0]}'")
            return
        }
    }
    val source = inputStream.bufferedReader().readText()
    try {
        val parser = Parser(source)
        val program = parser.parse()
        println(program.evaluate())
    } catch (error: LangException) {
        println(error.message)
    }
}