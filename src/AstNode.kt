typealias Operation = (Int, Int) -> Int
typealias Context = HashMap<String, Int>

sealed class AstNode {
    var parent: AstNode? = null
    abstract val loc: Location

    abstract fun toText(builder: StringBuilder)

    fun evaluate() = evaluate(Context())
    abstract fun evaluate(context: Context): Int

    open fun findIdentifier(idName: String): Identifier? {
        return parent?.findIdentifier(idName)
    }
}

class Program(private val last: Expression) : AstNode() {
    override val loc
            by lazy {
                val firstStmtLoc = (functions.firstOrNull() ?: last).loc
                pointLocation(firstStmtLoc)
            }
    private val functions = mutableListOf<Function>()

    init {
        last.parent = this
    }

    fun add(function: Function) {
        functions.add(function)
        function.parent = this
    }

    override fun toText(builder: StringBuilder) {
        functions.forEach { it.toText(builder); builder.append('\n') }
        last.toText(builder)
    }

    override fun evaluate(context: Context) = last.evaluate(context)

    override fun findIdentifier(idName: String): Identifier? {
        return functions
            .asSequence()
            .mapNotNull { it.name.findIdentifier(idName) }
            .firstOrNull()
    }
}

class Function(
    internal val name: Identifier,
    private val body: Expression
) : AstNode() {
    override val loc = Location(name.loc.source, name.loc.start, body.loc.end + 1, name.loc.lineNo)
    internal val params = mutableListOf<Identifier>()

    init {
        name.parent = this
        body.parent = this
    }

    fun add(param: Identifier) {
        params.add(param)
        param.parent = this
    }

    override fun toText(builder: StringBuilder) {
        name.toText(builder)
        builder.append('(')
        params.forEach { it.toText(builder); builder.append(',') }
        builder.deleteCharAt(builder.length - 1)
        builder.append(")={")
        body.toText(builder)
        builder.append('}')
    }

    override fun evaluate(context: Context) = body.evaluate(context)

    override fun findIdentifier(idName: String): Identifier? {
        return name.findIdentifier(idName) ?: super.findIdentifier(idName)
    }
}

sealed class Expression() : AstNode()

class Identifier(override val loc: Location) : Expression() {
    val name = loc.getValue()

    override fun evaluate(context: Context): Int {
        return context[name] ?: throw ParameterNotFoundException(loc)
    }

    override fun toText(builder: StringBuilder) {
        builder.append(name)
    }

    override fun findIdentifier(idName: String) = when (idName) {
        this.name -> this
        else -> null
    }
}

class ConstantExpression(negate: Boolean, numLoc: Location) : Expression() {
    override val loc: Location
    private val value: Int

    init {
        val abs = numLoc.getValue().toInt()
        if (negate) {
            value = abs * -1
            loc = Location(numLoc.source, numLoc.start - 1, numLoc.end, numLoc.lineNo)
        } else {
            value = abs
            loc = numLoc
        }
    }

    override fun evaluate(context: Context) = value

    override fun toText(builder: StringBuilder) {
        builder.append(value)
    }

    override fun findIdentifier(idName: String): Identifier? = null
}

class BinaryExpression(
    private val operation: Operation,
    private val opSymbol: String,
    private val operand1: Expression,
    private val operand2: Expression
) : Expression() {
    override val loc = Location(
        operand1.loc.source,
        operand1.loc.start - 1,
        operand2.loc.end + 1,
        operand1.loc.lineNo
    )

    init {
        operand1.parent = this
        operand2.parent = this
    }

    override fun evaluate(context: Context) =
        try {
            operation(
                operand1.evaluate(context),
                operand2.evaluate(context)
            )
        } catch (error: RuntimeException) {
            throw LangRuntimeError(loc)
        }

    override fun toText(builder: StringBuilder) {
        builder.append('(')
        operand1.toText(builder)
        builder.append(opSymbol)
        operand2.toText(builder)
        builder.append(')')
    }
}

class IfExpression(
    private val cond: Expression,
    private val truthCase: Expression,
    private val falsityCase: Expression
) : Expression() {
    override val loc = Location(
        cond.loc.source,
        cond.loc.start - 1,
        falsityCase.loc.end + 1,
        cond.loc.lineNo
    )

    init {
        cond.parent = this
        truthCase.parent = this
        falsityCase.parent = this
    }

    override fun evaluate(context: Context) = when {
        cond.evaluate(context) != 0 -> truthCase.evaluate(context)
        else -> falsityCase.evaluate(context)
    }

    override fun toText(builder: StringBuilder) {
        builder.append('[')
        cond.toText(builder)
        builder.append("]?{")
        truthCase.toText(builder)
        builder.append("}:{")
        falsityCase.toText(builder)
        builder.append('}')
    }
}

class CallExpression(private val funcName: Identifier) : Expression() {
    override val loc by lazy {
        val end = when {
            args.isEmpty() -> funcName.loc.end + 2 // name + two parens
            else -> args.last().loc.end + 1 // args + 1 paren
        }
        Location(funcName.loc.source, funcName.loc.start, end, funcName.loc.lineNo)
    }
    private val args = mutableListOf<Expression>()
    private val correspondingFunction: Function
            by lazy {
                val identifier = findIdentifier(funcName.name)
                    ?: throw FunctionNotFoundException(funcName.loc)
                if (identifier.parent !is Function) {
                    throw NotCallableException(funcName.loc)
                }
                identifier.parent as Function
            }

    init {
        funcName.parent = this
    }

    fun add(arg: Expression) {
        args.add(arg)
        arg.parent = this
    }

    override fun evaluate(context: Context): Int {
        if (args.size != correspondingFunction.params.size) {
            throw ArgumentMismatchException(funcName.loc)
        }
        val newContext = Context()
        correspondingFunction.params
            .map(Identifier::name)
            .zip(args.map { it.evaluate(context) })
            .forEach { (k, v) -> newContext[k] = v }
        return correspondingFunction.evaluate(newContext)
    }

    override fun toText(builder: StringBuilder) {
        funcName.toText(builder)
        builder.append('(')
        assert(args.size > 0)
        args.forEach { it.toText(builder); builder.append(',') }
        builder.deleteCharAt(builder.length - 1)
        builder.append(')')
    }
}
